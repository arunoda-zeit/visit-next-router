const drupalSRC = "https://visit.marioiliev.com";

const api = {
  drupal: {
    url: drupalSRC,
    get: {
      url: path => `${drupalSRC}${path}?_format=json`,
      all_menus: `${drupalSRC}/jsonapi/menu_link_content/menu_link_content`,
      paragraph: name => `${drupalSRC}/jsonapi/paragraph/${name}`
    },
    post: {}
  },
  localStorage: {
    set: (key, data) => {
      if (typeof data === "object") {
        data = JSON.stringify(data);
      }

      localStorage.setItem(key, data);
    },
    get: key => {
      let result;

      try {
        result = JSON.parse(localStorage.getItem(key));
      } catch (e) {
        result = localStorage.getItem(key);
      }

      return result;
    },
    remove: key => localStorage.removeItem(key)
  },
  prepareContent: string => string.replace(/src="\//g, `src="${drupalSRC}/`),
  isEmailValid: email => {
    const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(String(email).toLowerCase());
  },
  capitalizeFirstLetter: string => {
    return string[0].toUpperCase() + string.substr(1);
  }
};

export default api;
