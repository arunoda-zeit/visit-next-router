Install: `npm install`

Start: `npm run dev`

Node version used in the project: 8.11.1
For all other run scripts, take a look at package.json

The default Drupal connection is on https://visit.marioiliev.com/  
Drupal user/pass: admin/admin
