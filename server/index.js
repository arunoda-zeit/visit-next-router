const compression = require("compression");
const express = require("express");
const next = require("next");
const { parse } = require("url");
const dev = process.env.NODE_ENV !== "production";
const app = next({ dir: "src", dev });

// *** START of code that should be imported from "routes.js"
// Problem that must be solved: "routes.js" can't be imported because of an error "Unexpected export in routes.js"
//const routes = require("./routes");
const routes = require("next-routes")();
const Router = routes.Router;
const Link = routes.Link;

routes.add("/:splat*", "Router");
// *** END of code that should be imported from "routes.js"

app.prepare().then(() => {
  const server = express();

  // Add gzip compression.
  if (process.env.NODE_ENV === "production") {
    server.use(compression());
  }

  // Catch any requests to static files not served by next.
  server.use(/^(?!\/_next)(.*)\.(.*)/, (req, res) => {
    app.render404(req, res, parse(req.url, true));
  });

  server.use(routes.getRequestHandler(app));

  server.listen(3000);
});
