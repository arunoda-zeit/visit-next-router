import React, { Component } from "react";
import axios from "axios";

import api from "../../utilities/api";
import DefaultLayout from "./layouts/Default";
import PageLayout from "./layouts/PageLayout";
import ArticleLayout from "./layouts/ArticleLayout";
import Page404 from "./layouts/Page404";
import Loading from "./content/Loading";

class Router extends Component {
  constructor(props) {
    super(props);

    this.state = {
      page: false,
      pageNotFound: false
    };

    this.dummyMenu = {
      menu: [
        {
          url: "/",
          title: "Home"
        },
        {
          url: "/about-us",
          title: "About us"
        },
        {
          url: "/team",
          title: "Team"
        },
        {
          url: "/accommodation",
          title: "Accommodation"
        }
      ]
    };
  }

  pageCheckWithDrupal = () => {
    const path = this.props.url.asPath;

    axios
      .get(api.drupal.get.url(path))
      .then(response => this.setState({ page: response, pageNotFound: false }))
      .catch(() => this.setState({ pageNotFound: true }));
  };

  componentDidMount() {
    //console.log("Data recieved by the Router: ", this.props);
    this.pageCheckWithDrupal();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.url.asPath !== this.props.url.asPath) {
      this.pageCheckWithDrupal();
    }
  }

  render() {
    if (this.state.pageNotFound) {
      return <Page404 />;
    } else if (!this.state.page) {
      return <Loading activeColor="#ED1C24" baseColor="#FFF" />;
    } else {
      const pageType = this.state.page.data.type[0].target_id.toUpperCase();
      const pageData = Object.assign({}, this.state.page, { router: this.props.url }, this.dummyMenu);

      switch (pageType) {
        case "PAGE": {
          return <PageLayout {...pageData} />;
        }
        case "ARTICLE": {
          return <ArticleLayout {...pageData} />;
        }
        default: {
          return <DefaultLayout {...pageData} />;
        }
      }
    }
  }
}

export default Router;

// BEGIN: ORIGINAL CODE OF THIS PAGE

// import dynamic from 'next/dynamic';
// import { SingletonRouter } from 'next/router';
// import React, { ComponentType, StatelessComponent } from 'react';
// import { graphql } from 'react-apollo';
// import { compose } from 'recompose';
// import { IArticleFragment } from '~/components/Article';
// import { IPageFragment } from '~/components/Page';
// import withApollo from '~/shared/withApollo';
// import query from './query.gql';
//
// // tslint:disable-next-line:no-empty-interface
// export interface IRouterProps extends IRouterQueryChildProps {}
//
// export const Router: StatelessComponent<IRouterProps> = ({
//   entity,
//   loading,
// }) => {
//   if (loading) {
//     return null;
//   }
//
//   switch (entity && entity.__typename) {
//     case 'NodeArticle': {
//       // @ts-ignore
//       const Article = dynamic(import('~/components/Article'));
//       return <Article {...entity} />;
//     }
//
//     case 'NodePage': {
//       // @ts-ignore
//       const Page = dynamic(import('~/components/Page'));
//       return <Page {...entity} />;
//     }
//
//     default:
//       return null;
//   }
// };
//
// export interface IRouterQueryProps {
//   url: SingletonRouter;
// }
//
// export interface IRouterQueryData {
//   route: {
//     __typename: string;
//     entity: IPageFragment | IArticleFragment;
//   };
// }
//
// export interface IRouterQueryVariables {
//   path: string;
// }
//
// export interface IRouterQueryChildProps extends IRouterQueryProps {
//   entity: IPageFragment | IArticleFragment;
//   loading: boolean;
// }
//
// const withQuery = graphql<
//   IRouterQueryProps,
//   IRouterQueryData,
//   IRouterQueryVariables,
//   IRouterQueryChildProps
// >(query, {
//   options: props => ({
//     variables: {
//       path: props.url.asPath,
//     },
//   }),
//   props: ({ ownProps, data }) => ({
//     ...ownProps,
//     entity: data.route && data.route.entity,
//     loading: data.loading,
//   }),
// });
//
// export default compose(withApollo, withQuery)(Router);
