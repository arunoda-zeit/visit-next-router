import Head from "next/head";
import { Fragment } from "react";
import styled from "styled-components";

import Header from "../header/Header";
import Footer from "../footer/Footer";
import InnerHTML from "../content/InnerHTML";

const DefaultLayout = props => {
  //  console.log("Data recieved by the layout: ", props);

  const menuData = Object.assign({}, { data: props.menu }, { router: props.router });

  return (
    <Fragment>
      <Head>
        <title>{props.data.title[0].value}</title>
      </Head>
      <Header {...menuData} />
      <Wrap>
        <ContentWrap>
          <InnerHTML content={props.data.body[0].processed} />
        </ContentWrap>
      </Wrap>
      <Footer {...menuData} />
    </Fragment>
  );
};

export default DefaultLayout;

const Wrap = styled.div`
  width: 100%;
  max-width: 1140px;
  padding: 0 40px 40px 40px;
  margin: 85px auto 0 auto;
  background-color: #fff;
`;

const ContentWrap = styled.div`
  display: table-cell;
  vertical-align: middle;
  line-height: 1.5;
  font-size: 16.8px;
`;
