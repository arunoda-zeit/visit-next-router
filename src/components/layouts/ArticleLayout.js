import Head from 'next/head';
import { Fragment } from 'react';
import styled from 'styled-components';

import Link from '../Link';
import Header from '../header/Header';
import Footer from '../footer/Footer';
import InnerHTML from '../content/InnerHTML';
import ImageLinkBox from '../content/ImageLinkBox';
import HeroImage from '../content/HeroImage';

// This should be in a seprate file, with dynamic component loading
const componentsMap = {
  field_image_link_box: ImageLinkBox,
  field_hero_image: HeroImage,
};

const ArticleLayout = props => {
  //  console.log("Data recieved by the layout: ", props);
  const menuData = Object.assign(
    {},
    { data: props.menu },
    { router: props.router }
  );
  const paragraphs = [];
  const hero = [];

  for (var i in props.data) {
    if (i in componentsMap && props.data[i].length) {
      props.data[i].map(item => {
        const Item = componentsMap[i];

        if (i === 'field_hero_image') {
          hero.push(<Item {...item} name={i} key={item.target_uuid} />);
        } else {
          paragraphs.push(<Item {...item} name={i} key={item.target_uuid} />);
        }
      });
    }
  }

  return (
    <Fragment>
      <Head>
        <title>{props.data.title[0].value}</title>
      </Head>
      <Header {...menuData} />

      <Wrap>
        {hero}
        <Inner>
          <ContentWrap>
            <InnerHTML content={props.data.body[0].processed} />
          </ContentWrap>
          <Aside>{paragraphs}</Aside>
        </Inner>
      </Wrap>

      <Footer {...menuData} />
    </Fragment>
  );
};

export default ArticleLayout;

const Wrap = styled.div`
  width: 100%;
  margin: 85px 0 0 0;
  line-height: 1.5;
  font-size: 16.8px;
`;

const Inner = styled.div`
  display: table;
  width: 100%;
  max-width: 1220px;
  margin: 0 auto;
`;

const ContentWrap = styled.div`
  width: 70%;
  float: left;
  padding: 0 20px 30px 20px;
  box-sizing: border-box;
  background-color: #fff;
`;

const Aside = styled.div`
  width: 30%;
  float: right;
  padding: 0 0 0 20px;
  box-sizing: border-box;
`;
