import Head from 'next/head';
import { Fragment } from 'react';
import styled from 'styled-components';

import Header from '../header/Header';
import Footer from '../footer/Footer';
import InnerHTML from '../content/InnerHTML';
import Slideshow from '../content/Slideshow';

import MappaComponent from '../content/MappaComponent';

// This should be in a seprate file, with dynamic component loading
const componentsMap = {
  field_slideshow: Slideshow,
};

const PageLayout = props => {
  //  console.log("Data recieved by the layout: ", props);
  const menuData = Object.assign(
    {},
    { data: props.menu },
    { router: props.router }
  );
  const slideshow = [];
  const myMap = [];

  if (props.router.asPath === '/about-us') {
    myMap.push(<MappaComponent key="google-map" />);
  }

  for (var i in props.data) {
    if (i in componentsMap && props.data[i].length) {
      if (i === 'field_slideshow') {
        const Item = componentsMap[i];
        const slideShowConfig = {
          mainNavigation: true,
          bottomNavigation: true,
          transitionType: 5,
          slidesFullHeight: true,
          draggable: true,
        };

        slideshow.push(
          <Item data={props.data[i]} config={slideShowConfig} key="slideshow" />
        );
      }
    }
  }

  return (
    <Fragment>
      <Head>
        <title>{props.data.title[0].value}</title>
      </Head>
      <Header {...menuData} />
      <Wrap>
        <Inner>
          {slideshow}
          {myMap}
          <ContentWrap>
            <InnerHTML content={props.data.body[0].processed} />
          </ContentWrap>
        </Inner>
      </Wrap>
      <Footer {...menuData} />
    </Fragment>
  );
};

export default PageLayout;

const Wrap = styled.div`
  width: 100%;
  margin: 85px 0 0 0;
  line-height: 1.5;
  font-size: 16.8px;

  .rct-slider-mega-wrap {
    margin-top: 30px;
  }
`;

const Inner = styled.div`
  display: table;
  width: 100%;
  max-width: 1220px;
  margin: 0 auto;
  padding: 0 40px 40px 40px;
  background-color: #fff;
`;

const ContentWrap = styled.div`
  line-height: 1.5;
  font-size: 16.8px;
`;
