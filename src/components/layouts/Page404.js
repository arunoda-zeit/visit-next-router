import Head from "next/head";
import React from "react";
import styled from "styled-components";

import Logo from "../header/Logo";
import InnerHTML from "../content/InnerHTML";

const Page404 = props => (
  <Wrap>
    <Head>
      <title>VisitDenmark - Page not found</title>
    </Head>
    <Logo />
    <ContentWrap>
      <InnerHTML content="404 Page not found" />
    </ContentWrap>
  </Wrap>
);
export default Page404;

const Wrap = styled.div`
  margin: 0 auto;
  width: 100%;
  max-width: 1140px;
  height: 100%;

  .main-logo {
    float: none;
    display: table;
    margin: 15px auto 0 auto;
  }
`;

const ContentWrap = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  font-size: 35px;
  transform: translate(-50%, -50%);
`;
