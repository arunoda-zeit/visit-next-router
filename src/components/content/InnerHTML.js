import React from "react";

import api from "../../../utilities/api";

const InnerHTML = props => {
  const content = api.prepareContent(props.content);

  return <div dangerouslySetInnerHTML={{ __html: content }} />;
};

export default InnerHTML;
