import { Component } from "react";
import styled from "styled-components";
import axios from "axios";

import api from "../../../utilities/api";

class ImageLinkBox extends Component {
  state = {
    content: false,
    contentNotFound: false
  };

  componentDidMount() {
    // There is no working way to fetch the entire paragraph content with one API call. That's why this is commented.
    // axios
    //   .get(api.drupal.get.paragraph(this.props.name))
    //   .then(response => this.setState({ content: response, contentNotFound: false }))
    //   .catch(err => this.setState({ content: false, contentNotFound: true }));
  }

  render() {
    //console.log("Data recieved by the ImageLinkBox: ", this.props);
    let content = null;

    if (this.state.contentNotFound) {
      return null;
    } else if (!this.state.content) {
      content = <div>Loading...</div>;
    } else {
      content = <div>{this.state.content.data.attributes.field_link_to_page.title}</div>;
    }

    return (
      <Wrap>
        <Inner>{content}</Inner>
      </Wrap>
    );
  }
}

export default ImageLinkBox;

const Wrap = styled.div`
  margin: 0 0 20px 0;
  background-color: #fff;
  transition: box-shadow 0.3s ease-in-out;

  &:hover {
    box-shadow: 0 1px 16px 2px rgba(0, 0, 0, 0.1);
  }
`;

const Inner = styled.div`
  padding: 20px;
`;
