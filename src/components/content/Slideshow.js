import React, { Component } from "react";
import styled from "styled-components";
import animations from "../../styles/animations";
import optimizations from "../../styles/optimizations";

/*
Customizable slider component by: Mario Iliev
-----------------------------------------------------------------------------------------
Acceptable properties in the props data object (pass array of objects)
url: string (url to image)
title: string (HTML/JSX is NOT acceptable)

-----------------------------------------------------------------------------------------
Acceptable properties in the props config object

[ DIMENSIONS ----------------------------------------------------------------------------
  width: integer (pixels, Default: auto)
  height: integer (pixels, Default: taken from the DOM when first item is loaded)

[ NAVIGATIONS ---------------------------------------------------------------------------
  mainNavigation: boolean (show/hide, Default: disabled)
  bottomNavigation: boolean (show/hide, Default: disabled)
  bottomNavigationInside: boolean (inside/outside sliders wrap, Default: false (outside))
  bottomNavigationThumbs: boolean (Default: false)

[ CAPTIONS ------------------------------------------------------------------------------
  slideText: boolean (show/hide if text is provided, Default: disabled)

[ STYLING -------------------------------------------------------------------------------
  clearStyles: boolean (discard/use styles provided by the component, Default: true)
  backgroundColor: string expression (Example: "#000", Default: none)
  slidesFullHeight: boolean (if images should fill the container height, Default: false)
  buttonLeftImage: NODE (svg, img, JSX or React Component which returns any of those)
  buttonRightImage: NODE (svg, img, JSX or React Component which returns any of those)

[ FUNCTIONALITIES -----------------------------------------------------------------------
  pauseOnHover: boolean (Default: false)
  draggable: boolean (Works only for horizontal sliding transitions, Default: false)
  dragChangeWhen: boolean (Represent percent when slide will change, Default: 30)
  autoSlideInterval: integer (Default: 3000 milliseconds)
  autoSlide: boolean (Default: true)
  disableKeyboardNavigation: boolean (Default: false)

[ EFFECTS -------------------------------------------------------------------------------
  transitionDuration: integer (milliseconds)
  transitionType: integer (transition types are listed below)
  1 - Fade Out
  2 - Fade In
  3 - Slide Horizontal Hide
  4 - Slide Horizontal Hide & Show
  5 - Slide Horizontal Hide slower & Show
  6 - Slide Vertical Hide
  7 - Slide Vertical Hide & Show
  8 - Zoom Out Fading
  9 - Zoom In Fading
-----------------------------------------------------------------------------------------
*/

class SlideshowDefault extends Component {
  constructor(props) {
    super(props);

    this.state = {
      paused: false,
      currentItemId: 1,
      nextItemId: 2,
      previousItemId: props.data.length,
      nextSlideInterval: false,
      firstItemLoaded: false,
      animationTriggered: false,
      slideDraggedPosition: false,
      dragging: false,
      dragChangeItem: false,
      nextItemDirection: "next"
    };
    this.styleType = this.props.config.clearStyles ? "" : " default-style";
    this.slider = {
      transitions: {
        get: integer => (integer in this.slider.transitions ? this.slider.transitions[integer].name : "none"),
        1: { name: "fade", draggable: false },
        2: { name: "fade2", draggable: false },
        3: { name: "slide-hor-type1", draggable: true },
        4: { name: "slide-hor-type2", draggable: true },
        5: { name: "slide-hor-type3", draggable: true },
        6: { name: "slide-ver-type1", draggable: false },
        7: { name: "slide-ver-type2", draggable: false },
        8: { name: "zoom-out-fade", draggable: false },
        9: { name: "zoom-in-fade", draggable: false }
      },
      dragArea: {
        get: () => {
          if (this.props.config.draggable !== false) {
            return <DragArea className={`rct-slider-drag-surface${this.mouseIsDown ? " drag-active" : ""}`} />;
          }
        }
      },
      createSlide: url => <img className="rct-slider-item" src={url} alt="" />,
      mainNavigation: {
        get: () => {
          if (this.props.config.mainNavigation) {
            const btnStyleType = this.props.config.clearStyles || this.props.config.buttonLeftImage || this.props.config.buttonRightImage ? "" : " default-style";
            const buttonPrevIcon = this.props.config.buttonLeftImage || <ButtonIcon className={`rct-slider-nav-btn-icon prev${btnStyleType}`} />;
            const buttonNextIcon = this.props.config.buttonRightImage || <ButtonIcon className={`rct-slider-nav-btn-icon next${btnStyleType}`} />;

            return (
              <MainButtonWrap className="rct-slider-nav-wrap">
                <MainButton className={`rct-slider-nav-btn${btnStyleType} prev`} onClick={() => this.changeSlide("prev")}>
                  {buttonPrevIcon}
                </MainButton>
                <MainButton className={`rct-slider-nav-btn${btnStyleType} next`} onClick={() => this.changeSlide("next")}>
                  {buttonNextIcon}
                </MainButton>
              </MainButtonWrap>
            );
          }
        }
      },
      bottomNavigation: {
        items: [],
        get: position => {
          if (this.props.config.bottomNavigation) {
            if ((this.props.config.bottomNavigationInside && position === "inside") || (!this.props.config.bottomNavigationInside && position === "outside")) {
              return (
                <BottomNavigationWrap className={`rct-slider-bottom-nav-wrap positioned-${position}${this.styleType}`}>
                  <BottomNavigationInner className={`rct-slider-bottom-nav-inner${this.styleType}`}>{this.slider.bottomNavigation.items}</BottomNavigationInner>
                </BottomNavigationWrap>
              );
            }
          }
        }
      }
    };
  }

  changeSlide = (target, skipAnimation, additionalStateProps) => {
    let nextItemId;
    let currentItemId;
    let previousItemId;
    let nextItemDirection;

    if (typeof target === "number") {
      currentItemId = target;
      previousItemId = this.state.currentItemId;
      nextItemDirection = currentItemId > this.state.currentItemId ? "next" : "prev";

      if (nextItemDirection === "next") {
        nextItemId = currentItemId + 1 > this.props.data.length ? 1 : currentItemId + 1;
      } else {
        nextItemId = this.state.currentItemId;
      }
    } else {
      nextItemDirection = target || "next";

      if (nextItemDirection === "next") {
        currentItemId = this.state.currentItemId + 1 > this.props.data.length ? 1 : this.state.currentItemId + 1;
      } else {
        currentItemId = this.state.currentItemId - 1 === 0 ? this.props.data.length : this.state.currentItemId - 1;
      }

      nextItemId = currentItemId + 1 > this.props.data.length ? 1 : currentItemId + 1;
      previousItemId = currentItemId - 1 === 0 ? this.props.data.length : currentItemId - 1;
    }

    if (currentItemId !== this.state.currentItemId) {
      const newState = {
        nextItemId: nextItemId,
        currentItemId: currentItemId,
        previousItemId: previousItemId,
        nextItemDirection: nextItemDirection,
        slideDraggedPosition: false,
        ...additionalStateProps
      };

      if (!skipAnimation && this.props.config && this.props.config.transitionType) {
        newState["animationTriggered"] = true;
      }

      if (!this.state.animationTriggered) {
        if (!this.state.paused && (!this.props.config || (this.props.config && this.props.config.autoSlide !== false))) {
          this.setStateAndAutoSlideInterval(newState);
        } else {
          this.setState(newState);
        }
      }
    }
  };

  setStateAndAutoSlideInterval(additionalStateProps) {
    const interval = this.props.config && this.props.config.autoSlideInterval ? this.props.config.autoSlideInterval : 3000;

    clearInterval(this.state.nextSlideInterval);

    this.setState({
      nextSlideInterval: setInterval(this.changeSlide, interval),
      ...additionalStateProps
    });
  }

  onFirstItemLoaded = () => {
    if (!this.slider.width) {
      this.slider.width = this.SlidesWrapNode.clientWidth || false;
    }

    if (!this.state.paused && (!this.props.config || (this.props.config && this.props.config.autoSlide !== false))) {
      this.setStateAndAutoSlideInterval({ firstItemLoaded: true });
    } else {
      this.setState({ firstItemLoaded: true });
    }
  };

  onMouseEnter = () => {
    if (this.props.config && this.props.config.pauseOnHover && this.props.config.autoSlide !== false) {
      clearInterval(this.state.nextSlideInterval);

      this.setState({ paused: true });
    }
  };

  onMouseLeave = () => {
    if (this.mouseIsDown) {
      this.onMouseUp();
    } else if (this.state.paused) {
      if (this.props.config && this.props.config.autoSlide !== false) {
        this.setStateAndAutoSlideInterval({ paused: false });
      } else {
        this.setState({ paused: false });
      }
    }
  };

  onKeyDown = e => {
    if (!this.state.dragging && !this.state.animationTriggered && !this.state.slideDraggedPosition) {
      if (e.keyCode === 37) {
        this.changeSlide("prev");
      } else if (e.keyCode === 39) {
        this.changeSlide("next");
      }
    }
  };

  onMouseDown = e => {
    if (
      this.props.config &&
      this.props.config.draggable !== false &&
      !this.state.animationTriggered &&
      this.slider.transitions[this.props.config.transitionType] &&
      this.slider.transitions[this.props.config.transitionType].draggable
    ) {
      const target = e && e.target && e.target.className && e.target.className.length ? e.target.className : false;

      if (
        target &&
        (target.indexOf("rct-slider-nav-wrap") !== -1 ||
          target.indexOf("rct-slider-item") !== -1 ||
          target.indexOf("rct-slider-drag-surface") !== -1 ||
          target.indexOf("rct-slider-bottom-nav-wrap") !== -1)
      ) {
        this.mouseIsDown = true;

        if (window.getSelection) {
          window.getSelection().removeAllRanges();
        } else if (document.selection) {
          document.selection.empty();
        }

        document.addEventListener("mousemove", this.onMouseMove, false);
        clearInterval(this.state.nextSlideInterval);
      }
    }
  };

  onMouseUp = e => {
    const newState = {};

    this.mouseIsDown = false;
    this.dragStartedAt = false;

    if (this.state.dragging) {
      let shouldChangeItem = false;

      if (this.slider.width) {
        const percent = this.props.config && this.props.config.dragChangeWhen ? this.props.config.dragChangeWhen : 30;

        // Change when dragged beyond specific percent
        if (Math.round(Math.abs((this.state.dragging / this.slider.width) * 100)) >= percent) {
          shouldChangeItem = true;
        }
      } else {
        // Fallback: change when dragged beyond specific pixels
        if (this.state.dragging <= -150 || this.state.dragging >= 150) {
          shouldChangeItem = true;
        }
      }

      newState["dragging"] = false;
      newState["dragChangeItem"] = shouldChangeItem;
      newState["slideDraggedPosition"] = this.state.dragging;
    }

    if (this.mouseMoveInterval) {
      clearInterval(this.mouseMoveInterval);
      this.mouseMoveInterval = false;
    }

    if (Object.keys(newState).length) {
      if (this.props.config && this.props.config.autoSlide !== false && !this.state.paused) {
        this.setStateAndAutoSlideInterval(newState);
      } else {
        this.setState(newState);
      }
    }

    document.removeEventListener("mousemove", this.onMouseMove, false);
  };

  onMouseMove = e => {
    if (this.mouseIsDown && !this.state.dragChangeItem && e.buttons) {
      const currentPosition = Math.abs(e.offsetX);

      if (!this.dragStartedAt) {
        this.dragStartedAt = currentPosition;
      }

      this.dragPosition = currentPosition - this.dragStartedAt;

      if (!this.lastDragPosition) {
        this.lastDragPosition = this.dragPosition;
      }

      if (!this.mouseMoveInterval) {
        this.mouseMoveInterval = setInterval(() => {
          if (this.lastDragPosition !== this.dragPosition) {
            this.lastDragPosition = this.dragPosition;

            this.setState({
              dragging: this.dragPosition
            });
          }
        }, 10);
      }
    } else {
      this.onMouseUp();
    }
  };

  onSlideAnimationEnd = () => {
    if (this.state.animationTriggered) {
      this.setState({
        nextItemId: this.state.currentItemId + 1 > this.props.data.length ? 1 : this.state.currentItemId + 1,
        previousItemId: this.state.currentItemId - 1 === 0 ? this.props.data.length : this.state.currentItemId - 1,
        animationTriggered: false
      });
    }
  };

  onSlideTransitionEnd = () => {
    if (this.state.slideDraggedPosition) {
      const newState = {
        slideDraggedPosition: false,
        dragChangeItem: false
      };

      if (this.state.dragChangeItem) {
        const direction = this.state.slideDraggedPosition < 0 ? "next" : "prev";

        this.changeSlide(direction, true, newState);
      } else {
        if (!this.state.paused && (!this.props.config || (this.props.config && this.props.config.autoSlide !== false))) {
          this.setStateAndAutoSlideInterval(newState);
        } else {
          this.setState(newState);
        }
      }
    }
  };

  componentDidMount() {
    const config = this.props.config || {};

    if (!config.disableKeyboardNavigation) {
      document.addEventListener("keydown", this.onKeyDown, false);
    }

    if (
      this.props.config.draggable &&
      this.props.config.transitionType &&
      this.slider.transitions[this.props.config.transitionType] &&
      this.slider.transitions[this.props.config.transitionType].draggable
    ) {
      window.addEventListener("blur", this.onMouseUp, false);
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.config.autoSlide !== this.props.config.autoSlide) {
      if (this.props.config.autoSlide === false) {
        clearInterval(this.state.nextSlideInterval);
      } else {
        this.setStateAndAutoSlideInterval();
      }
    }

    if (prevProps.config.clearStyles !== this.props.config.clearStyles) {
      this.styleType = this.props.config.clearStyles ? "" : " default-style";
    }

    if (this.props.config.disableKeyboardNavigation) {
      document.removeEventListener("keydown", this.onKeyDown, false);
    } else {
      document.addEventListener("keydown", this.onKeyDown, false);
    }

    if (
      this.props.config.draggable &&
      this.props.config.transitionType &&
      this.slider.transitions[this.props.config.transitionType] &&
      this.slider.transitions[this.props.config.transitionType].draggable
    ) {
      window.addEventListener("blur", this.onMouseUp, false);
    } else {
      window.removeEventListener("blur", this.onMouseUp, false);
    }
  }

  componentWillUnmount() {
    clearInterval(this.state.nextSlideInterval);

    window.removeEventListener("keydown", this.onKeyDown, false);
    window.removeEventListener("blur", this.onMouseUp, false);
  }

  render() {
    const config = this.props.config || {};
    let megaWrapClass = `rct-slider-mega-wrap`;
    let allItemsWrapStyles = {};

    this.slider["slides"] = [];
    this.slider["bottomNavigation"].items = [];

    if (this.props.className) {
      megaWrapClass += ` ${this.props.className}`;
    }

    if (config.draggable && config.transitionType && this.slider.transitions[config.transitionType].draggable) {
      megaWrapClass += " is-draggable";
    }

    if (config.height) {
      allItemsWrapStyles.height = `${config.height}px`;
    } else if (this.SlidesWrapNode && this.SlidesWrapNode.clientHeight) {
      allItemsWrapStyles.height = `${this.SlidesWrapNode.clientHeight}px`;
    }

    if (config.width) {
      allItemsWrapStyles.width = `${config.width}px`;
    } else if (this.SlidesWrapNode && this.SlidesWrapNode.clientWidth) {
      allItemsWrapStyles.width = `${this.SlidesWrapNode.clientWidth}px`;
    }

    if (config.backgroundColor) {
      allItemsWrapStyles["backgroundColor"] = config.backgroundColor;
    }

    if (this.state.firstItemLoaded) {
      let transitionEffect;
      let transitionDirection;
      let dragDirection;

      if (config.transitionType) {
        transitionEffect = `effect-${this.slider.transitions.get(config.transitionType)}`;
        transitionDirection = this.state.nextItemDirection === "next" ? "dir-right" : "dir-left";
      }

      if (this.state.dragging || this.state.slideDraggedPosition) {
        dragDirection = this.state.dragging < 0 || this.state.slideDraggedPosition < 0 ? "left" : "right";
      }

      this.props.data.forEach((item, index) => {
        const thisIndex = index + 1;
        let itemWrapClass = "";
        let itemWrapStyles = {};
        let sliderItemHeadline;

        if (thisIndex === this.state.currentItemId) {
          itemWrapClass = " active";
        }

        if (config.transitionType) {
          // Change Slide with Animation
          if (this.state.animationTriggered && !this.state.dragChangeItem) {
            if (this.state.nextItemDirection === "next") {
              if (thisIndex === this.state.previousItemId && this.state.firstItemLoaded) {
                itemWrapClass += ` transition-hide ${transitionEffect} ${transitionDirection}`;
              }
            } else {
              if (thisIndex === this.state.nextItemId && this.state.firstItemLoaded) {
                itemWrapClass += ` transition-hide ${transitionEffect} ${transitionDirection}`;
              }
            }

            if (thisIndex === this.state.currentItemId) {
              itemWrapClass += ` transition-show ${transitionEffect} ${transitionDirection}`;
            }

            if (config.transitionDuration && (thisIndex === this.state.previousItemId || thisIndex === this.state.currentItemId || thisIndex === this.state.nextItemId)) {
              itemWrapStyles["animationDuration"] = `${config.transitionDuration / 1000}s`;
            }
          }

          // Dragging
          if (this.state.dragging || this.state.slideDraggedPosition) {
            const transitionName = this.slider.transitions[config.transitionType].name;

            // ------------------------- Current Item -------------------------
            if (thisIndex === this.state.currentItemId) {
              itemWrapClass += " readyForTransition";

              if (this.state.dragChangeItem) {
                itemWrapStyles["transition"] = "left 0.5s ease-out";
                itemWrapStyles["left"] = this.state.slideDraggedPosition < 0 ? "-100%" : "100%";
              } else {
                if (this.state.dragging) {
                  itemWrapStyles["left"] = `${this.state.dragging}px`;
                } else if (this.state.slideDraggedPosition) {
                  itemWrapStyles["left"] = 0;
                  itemWrapStyles["transition"] = "left 0.3s ease-in-out";
                }
              }
            }

            // ------------------------- Next Item -------------------------
            if (dragDirection === "left" && thisIndex === this.state.nextItemId) {
              itemWrapStyles["display"] = "block";

              if (transitionName === "slide-hor-type2" || transitionName === "slide-hor-type3") {
                itemWrapClass += " readyForTransition";

                if (this.state.dragChangeItem) {
                  itemWrapStyles["transition"] = "left 0.5s ease-out";
                  itemWrapStyles["left"] = 0;
                } else {
                  if (this.state.dragging) {
                    itemWrapStyles["left"] = `${this.slider.width - Math.abs(this.state.dragging)}px`;
                  } else if (this.state.slideDraggedPosition) {
                    itemWrapStyles["left"] = "100%";
                    itemWrapStyles["transition"] = "left 0.3s ease-in-out";
                  }
                }
              }
            }

            // ------------------------- Previous Item -------------------------
            if (dragDirection === "right" && thisIndex === this.state.previousItemId) {
              itemWrapStyles["display"] = "block";

              if (transitionName === "slide-hor-type2" || transitionName === "slide-hor-type3") {
                itemWrapClass += " readyForTransition";

                if (this.state.dragChangeItem) {
                  itemWrapStyles["transition"] = "left 0.5s ease-out";
                  itemWrapStyles["left"] = 0;
                } else {
                  if (this.state.dragging) {
                    itemWrapStyles["left"] = `${-(this.slider.width - Math.abs(this.state.dragging))}px`;
                  } else if (this.state.slideDraggedPosition) {
                    itemWrapStyles["left"] = "-100%";
                    itemWrapStyles["transition"] = "left 0.3s ease-in-out";
                  }
                }
              }
            }
          }
        }

        if (config.slideText && item.title) {
          sliderItemHeadline = <SlideHeadline className={`rct-slider-item-headline${this.styleType}`}>{item.title}</SlideHeadline>;
        }

        if (config.bottomNavigation) {
          let isActive = this.state.currentItemId === thisIndex ? " active" : "";

          if (config.bottomNavigationThumbs) {
            const wrapStyles = {
              maxWidth: `${100 / this.props.data.length}%`
            };

            this.slider.bottomNavigation.items.push(
              <BottomNavigationThumbWrap
                className={`rct-slider-bottom-nav-img-btn-wrap${this.styleType}${isActive}`}
                style={wrapStyles}
                onClick={() => this.changeSlide(thisIndex)}
                key={`btm-nav-${thisIndex}`}>
                {this.slider.createSlide(item.url)}
              </BottomNavigationThumbWrap>
            );
          } else {
            this.slider.bottomNavigation.items.push(
              <BottomNavigationButton
                className={`rct-slider-bottom-nav-btn${this.styleType}${isActive}`}
                onClick={() => this.changeSlide(thisIndex)}
                key={`btm-nav-${thisIndex}`}
              />
            );
          }
        }

        this.slider.slides.push(
          <ItemWrap
            className={`rct-slider-item-wrap${itemWrapClass}`}
            style={itemWrapStyles}
            key={`item-wrap-${thisIndex}`}
            onAnimationEnd={this.onSlideAnimationEnd}
            onTransitionEnd={this.onSlideTransitionEnd}>
            <ItemInner className="rct-slider-item-inner">
              {sliderItemHeadline}
              {this.slider.createSlide(item.url)}
            </ItemInner>
          </ItemWrap>
        );
      });
    } else {
      this.slider.slides = (
        <ItemWrap className="rct-slider-item-wrap active">
          <ItemInner className="rct-slider-item-inner">
            <img src={this.props.data[0].url} onLoad={this.onFirstItemLoaded} alt="Init slideshow after first item loads" />
          </ItemInner>
        </ItemWrap>
      );
    }

    return (
      <MegaWrap className={megaWrapClass}>
        <Wrap
          className={`rct-slider-wrap${config.slidesFullHeight && allItemsWrapStyles.height ? " full-height" : ""}`}
          style={allItemsWrapStyles}
          innerRef={div => (this.SlidesWrapNode = div)}
          onMouseDown={this.onMouseDown}
          onMouseUp={this.onMouseUp}
          onMouseEnter={this.onMouseEnter}
          onMouseLeave={this.onMouseLeave}>
          {this.slider.dragArea.get()}
          {this.slider.mainNavigation.get()}
          {this.slider.slides}
          {this.slider.bottomNavigation.get("inside")}
        </Wrap>
        {this.slider.bottomNavigation.get("outside")}
      </MegaWrap>
    );
  }
}

export default SlideshowDefault;

const MegaWrap = styled.div`
  * {
    user-select: none;

    &::selection {
      background-color: transparent;
    }
  }

  &.is-draggable {
    .rct-slider-nav-wrap,
    .rct-slider-item-inner img,
    .rct-slider-drag-surface {
      cursor: grab;

      &.drag-active {
        cursor: grabbing;
      }
    }
  }
`;
const Wrap = styled.div`
  position: relative;
  overflow: hidden;
  margin: 0 auto;

  &.full-height {
    .rct-slider-item-inner {
      position: absolute;
      height: 100%;
      width: 100%;

      img,
      iframe {
        height: 100%;
      }
    }
  }
`;
const ItemWrap = styled.div`
  display: none;
  width: 100%;
  height: 100%;
  background-color: inherit;

  ${optimizations.enableHardwareAcceleration};

  &.active {
    display: table;
  }
  &.transition-hide {
    display: table;
    position: absolute;
    top: 0;
    left: 0;
    z-index: 1;
  }
  &.readyForTransition {
    position: absolute;
    top: 0;
    z-index: 2;
  }

  &.effect-fade {
    ${optimizations.willChange("opacity")};

    &.transition-hide {
      animation: ${animations.fadeOut} 0.3s forwards ease;
    }
  }

  &.effect-fade2 {
    ${optimizations.willChange("opacity")};

    &.transition-hide {
      display: none;
    }
    &.transition-show {
      animation: ${animations.fadeInFromHalf} 0.5s forwards ease;
    }
  }

  &.effect-slide-hor-type1 {
    ${optimizations.willChange("left")};

    &.transition-hide {
      &.dir-left {
        animation: ${animations.slideHideToRight} 0.7s forwards ease-in-out;
      }
      &.dir-right {
        animation: ${animations.slideHideToLeft} 0.7s forwards ease-in-out;
      }
    }
  }

  &.effect-slide-hor-type2 {
    ${optimizations.willChange("left")};

    &.transition-hide {
      &.dir-left {
        animation: ${animations.slideHideToRight} 0.7s forwards ease-in-out;
      }
      &.dir-right {
        animation: ${animations.slideHideToLeft} 0.7s forwards ease-in-out;
      }
    }
    &.transition-show {
      position: absolute;
      z-index: 2;

      &.dir-left {
        animation: ${animations.slideShowFromRight} 0.7s forwards ease-in-out;
      }
      &.dir-right {
        animation: ${animations.slideShowFromLeft} 0.7s forwards ease-in-out;
      }
    }
  }

  &.effect-slide-hor-type3 {
    ${optimizations.willChange("left")};

    &.transition-hide {
      &.dir-left {
        animation: ${animations.slidePartialHideToRight} 0.7s forwards ease-in-out;
      }
      &.dir-right {
        animation: ${animations.slidePartialHideToLeft} 0.7s forwards ease-in-out;
      }
    }
    &.transition-show {
      position: absolute;
      z-index: 2;

      &.dir-left {
        animation: ${animations.slideShowFromRight} 0.7s forwards ease-in-out;
      }
      &.dir-right {
        animation: ${animations.slideShowFromLeft} 0.7s forwards ease-in-out;
      }
    }
  }

  &.effect-slide-ver-type1 {
    ${optimizations.willChange("top")};

    &.transition-hide {
      &.dir-right {
        animation: ${animations.slideHideToBottom} 0.7s forwards ease-in-out;
      }
    }
    &.transition-show {
      &.dir-left {
        position: absolute;
        z-index: 2;
        animation: ${animations.slideShowFromBottom} 0.7s forwards ease-in-out;
      }
    }
  }

  &.effect-slide-ver-type2 {
    ${optimizations.willChange("top")};

    &.transition-hide {
      &.dir-left {
        animation: ${animations.slideHideToTop} 0.7s forwards ease-in-out;
      }
      &.dir-right {
        animation: ${animations.slideHideToBottom} 0.7s forwards ease-in-out;
      }
    }
    &.transition-show {
      position: absolute;
      z-index: 2;

      &.dir-left {
        animation: ${animations.slideShowFromBottom} 0.7s forwards ease-in-out;
      }
      &.dir-right {
        animation: ${animations.slideShowFromTop} 0.7s forwards ease-in-out;
      }
    }
  }

  &.effect-zoom-out-fade {
    ${optimizations.willChange("transform, opacity")};

    &.transition-hide {
      animation: ${animations.zoomOutFading} 0.5s forwards ease;
    }
  }

  &.effect-zoom-in-fade {
    ${optimizations.willChange("transform, opacity")};

    &.transition-hide {
      animation: ${animations.zoomInFading} 0.5s forwards ease;
    }
  }
`;
const ItemInner = styled.div`
  display: table-cell;
  vertical-align: middle;

  &.full-height {
    position: absolute;
    height: 100%;

    img,
    iframe {
      height: 100%;
    }
  }

  img,
  iframe {
    display: block;
    width: 100%;
  }
`;
const SlideHeadline = styled.div`
  &.default-style {
    text-align: center;
    position: absolute;
    box-sizing: border-box;
    top: 50%;
    width: 100%;
    padding: 0 120px;
    transform: translateY(-60%);
    font-size: 50px;
    color: #fff;
    line-height: 1em;
    font-weight: 100;
    text-shadow: 0px 3px 5px rgba(0, 0, 0, 0.5);
  }
`;
const MainButtonWrap = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 4;
`;
const DragArea = styled.div`
  display: none;
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 6;

  &.drag-active {
    display: block;
  }
`;
const MainButtonWidth = 50;
const MainButtonHeight = 50;
const MainButton = styled.div`
  position: absolute;
  cursor: pointer;
  z-index: 1;

  img,
  svg {
    max-width: 100%;
    max-height: 100%;
    min-width: 10px;
  }

  &.default-style {
    top: 50%;
    width: ${MainButtonWidth}px;
    height: ${MainButtonHeight}px;
    margin: -${MainButtonHeight / 2}px 0 0 0;

    &.prev {
      left: 30px;
    }
    &.next {
      right: 30px;
    }
  }
`;
const ButtonIcon = styled.div`
  &.default-style {
    top: 10px;
    width: 0;
    height: 0;
    padding: 12px;
    position: absolute;
    display: inline-block;
    border: solid #f9f9f9;
    border-width: 0 5px 5px 0;
    box-shadow: 4px 4px 3px rgba(0, 0, 0, 0.03);

    &.prev {
      left: 7px;
      transform: rotate(135deg);
    }
    &.next {
      right: 7px;
      transform: rotate(-45deg);
    }
  }
`;
const BottomNavigationWrap = styled.div`
  &.default-style {
    &.positioned-inside {
      position: absolute;
      bottom: 10px;
      width: 100%;
      z-index: 4;
    }
    &.positioned-outside {
      margin: 10px 0 0 0;
    }
  }
`;
const BottomNavigationInner = styled.div`
  &.default-style {
    display: table;
    margin: 0 auto;
  }
`;
const BottomNavigationButton = styled.div`
  &.default-style {
    width: 10px;
    height: 10px;
    float: left;
    cursor: pointer;
    margin: 0 0 0 10px;
    border-radius: 100%;
    background-color: #868686;
    transition: background-color 0.2s ease;

    &.active {
      background-color: #fb0a12;
    }
    &:hover {
      background-color: #fb0a12;
    }
    &:first-child {
      margin-left: 0;
    }
  }
`;
const BottomNavigationThumbWrap = styled.div`
  position: relative;
  cursor: pointer;

  &.default-style {
    display: inline-block;
    vertical-align: middle;
  }

  img {
    max-width: 100%;
  }
`;
