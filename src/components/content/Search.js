import React from "react";
import styled from "styled-components";

const Search = props => {
  return (
    <Wrap>
      <Input type="text" placeholder="Search" />
    </Wrap>
  );
};

export default Search;

const Wrap = styled.div`
  float: right;
  margin: 26px 0 0 0;
`;

const Input = styled.input`
  display: block;
  color: #737373;
  font-size: 13px;
  height: 24px;
  border: 1px solid #c7c7c7;
  border-radius: 30px;
  padding: 3px 20px;
  outline: none;
  transition: border-color 0.3s ease;

  &:hover,
  &:focus {
    border-color: #9ebcc5;
  }
`;
