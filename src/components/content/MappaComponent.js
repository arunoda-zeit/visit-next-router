import { Component, Fragment } from 'react';
import styled from 'styled-components';
import Script from 'react-load-script';

const key = 'AIzaSyDzp0vX87rgKLoKuEIYEhlCQUNyGSrMM1g';

class MappaComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mappaCreated: false,
    };
  }

  componentDidUpdate() {
    if (
      this.state.p5Loaded &&
      this.state.googleApi &&
      !this.state.mappaCreated
    ) {
      var googleMap = new google.maps.Map(this.googleMapDiv, {
        center: { lat: -34.397, lng: 150.644 },
        zoom: 8,
      });

      const createCanvas = p => {
        p.setup = () => {
          p.createCanvas(600, 310);
        };

        p.draw = () => {
          p.background(0);
          p.fill(255);
          p.ellipse(220, 100, 50, 50);
        };
      };

      const myp5 = new p5(createCanvas, this.canvasDiv);

      this.setState({ mappaCreated: true });
    }
  }

  render() {
    return (
      <Fragment>
        {/* <Script url="https://cdn.jsdelivr.net/npm/mappa-mundi@0.0.5" onLoad={() => this.setState({ mappaLoaded: true })} /> */}
        <Script
          url="https://cdnjs.cloudflare.com/ajax/libs/p5.js/0.6.1/p5.min.js"
          onLoad={() => this.setState({ p5Loaded: true })}
        />
        <Script
          url={`https://maps.googleapis.com/maps/api/js?key=${key}`}
          onLoad={() => this.setState({ googleApi: true })}
        />

        <Wrap className="mappa_wrap">
          <div className="mappa_inner">
            <h3>Mapche</h3>
            <CanvasArea innerRef={ref => (this.canvasDiv = ref)} />
            <GoogleMap innerRef={ref => (this.googleMapDiv = ref)} />
          </div>
        </Wrap>
      </Fragment>
    );
  }
}

export default MappaComponent;

const Wrap = styled.div`
  display: table;
  margin: 0 auto;
`;
const CanvasArea = styled.div``;
const GoogleMap = styled.div`
  height: 300px;
  width: 600px;
`;
