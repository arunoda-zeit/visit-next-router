import React from "react";
import styled from "styled-components";

const HeroImage = props => <Wrap img={props.url} height={props.height} />;

export default HeroImage;

const Wrap = styled.div`
  margin: 0 0 20px 0;
  height: ${props => props.height}px;
  background-image: url(${props => props.img});
  background-attachment: fixed;
  background-repeat: no-repeat;
  background-position: center 85px;
  background-size: auto;

  img {
    display: block;
    margin: 0 auto;
  }
`;
