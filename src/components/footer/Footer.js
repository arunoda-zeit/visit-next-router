import React from 'react';
import styled from 'styled-components';

import Menu from '../header/Menu';
import LogoIMG from '~/static/images/VisitHeart';

const Footer = props => {
  //console.log("Props received by Footer: ", props);

  return (
    <Wrap>
      <Inner>
        <Logo>
          <LogoIMG />
        </Logo>
        <Menu {...props} />
      </Inner>
    </Wrap>
  );
};

export default Footer;

const Wrap = styled.div`
  width: 100%;
  display: table;
  position: relative;
  background-color: #deebee;
`;

const Inner = styled.div`
  max-width: 1220px;
  margin: 0 auto;
  padding: 0 20px;

  .main-menu {
    float: none;
    display: table;
    border: 0;
    padding: 0;
    margin: 40px auto 0 auto;
  }
`;

const Logo = styled.div`
  position: absolute;
  top: -20px;
  left: 50%;
  width: 50px;
  height: 43px;
  border: 20px solid #deebee;
  margin-left: -45px;
  -moz-border-radius: 100%;
  -webkit-border-radius: 100%;
  border-radius: 100%;
  background-color: #deebee;
  box-sizing: content-box;
  z-index: 3;
`;
