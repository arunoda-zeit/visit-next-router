import React from "react";
import styled from "styled-components";

import Logo from "./Logo";
import Menu from "./Menu";
import Search from "../content/Search";

const Header = props => {
  //console.log("Props received by Header: ", props);

  return (
    <Wrap>
      <Inner>
        <Logo />
        <Menu {...props} />
        <Search />
      </Inner>
    </Wrap>
  );
};

export default Header;

const Wrap = styled.div`
  height: 85px;
  width: 100%;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 100;
  background-color: #fff;
  box-shadow: 2px 2px 10px 0 rgba(0, 0, 0, 0.05);
`;

const Inner = styled.div`
  max-width: 1220px;
  margin: 0 auto;
  padding: 0 20px;
`;
