import React from "react";
import styled from "styled-components";

import Link from "../Link";

const Menu = props => {
  const mainMenu = [];

  props.data.map(item => {
    const path = `${item.url}`;

    mainMenu.push(
      <li key={`key-${path}`}>
        <Link href={path} className={path === props.router.asPath ? "active" : null}>
          {item.title}
        </Link>
      </li>
    );
  });

  // PUSH A BROKEN URL, IN ORDER TO TEST 404 PAGE NOT FOUND
  mainMenu.push(
    <li key="pnf">
      <Link href="/this-page-does-not-exists">Page 404</Link>
    </li>
  );

  return (
    <Wrap className="main-menu">
      <ul>{mainMenu}</ul>
    </Wrap>
  );
};

export default Menu;

const Wrap = styled.div`
  float: left;
  height: 85px;
  margin: 0 0 0 30px;
  padding: 0 0 0 30px;
  border-left: 1px solid #eef3f5;

  ul {
    list-style: none;
    margin: 25px 0 0 0;
    padding: 0;

    li {
      display: inline-block;

      &:first-child {
        a {
          margin-left: 0;
        }
      }

      a {
        display: inline-block;
        color: #353535;
        text-decoration: none;
        margin-left: 35px;
        font-weight: 700;
        font-size: 13px;
        text-transform: uppercase;
        transition: color 0.3s ease;

        &:hover {
          color: #41728b;
        }
        &.active {
          color: #c4161c;
          cursor: default;
        }
      }
    }
  }
`;
