import React from "react";
import styled from "styled-components";

import Link from "../Link";
import LogoIMG from "~/static/images/logo.png";

const Logo = () => (
  <Wrap className="main-logo">
    <Link href="/">
      <img src={LogoIMG} alt="" />
    </Link>
  </Wrap>
);

export default Logo;

const Wrap = styled.div`
  float: left;
  margin: 15px 0 0 0;

  a,
  img {
    display: block;
    max-width: 180px;
  }
`;
