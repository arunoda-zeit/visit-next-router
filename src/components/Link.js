import React, { StatelessComponent } from 'react';
import { Link } from '~/routes';

const NextLink = ({ href, ...props }) => (
  <Link route={href}>
    <a {...props} href={href} />
  </Link>
);

// export interface ILinkProps {
//   href: string;
//   [key: string]: any;
// }
//
// const NextLink: StatelessComponent<ILinkProps> = ({ href, ...props }) => (
//   <Link route={href}>
//     <a {...props} href={href} />
//   </Link>
// );

export default NextLink;
