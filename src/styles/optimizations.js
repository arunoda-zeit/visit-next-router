import { css } from "styled-components";

const optimizators = {
  enableHardwareAcceleration: css`
    transform: translateZ(0);
  `,
  willChange: stringProperties =>
    css`
      will-change: ${stringProperties};
    `
};

export default optimizators;
