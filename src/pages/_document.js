import React from 'react';
import OriginalDocument, { Head, Main, NextScript } from 'next/document';
import {
  AmpScripts,
  AmpScriptsManager,
  headerBoilerplate,
} from 'react-amphtml/setup';
import { ServerStyleSheet } from 'styled-components';
import * as Amp from 'react-amphtml';
import favicon from '~/static/favicon.ico';
import normalize from 'normalize.css';
import globalStyle from '~/styles/index.css';

// These globals will be injected at run-time, thus allowing
// environment variables to be changed independent of the
// generated build. This is useful e.g. when deploying the
// same container into different environments.
const keys = ['API'];
const globals = keys
  .map(key => `window.__${key}__ = ${JSON.stringify(process.env[key])};`)
  .join(' ');

// AMP the entire website: https://www.ampproject.org/
const is_AMP = false;

export default class Document extends OriginalDocument {
  static getInitialProps({ req, renderPage }) {
    const sheet = new ServerStyleSheet();

    if (is_AMP) {
      const ampScripts = new AmpScripts();
      const page = renderPage(App => props =>
        sheet.collectStyles(
          <AmpScriptsManager ampScripts={ampScripts}>
            <App {...props} />
          </AmpScriptsManager>
        )
      );

      const ampScriptTags = ampScripts.getScriptElements();
      const title = page.head
        .filter(({ type }) => type === 'title')
        .slice(0, 1) || <title>ampreact</title>;
      const ampStyleTag = (
        <style
          amp-custom=""
          dangerouslySetInnerHTML={{
            __html: sheet
              .getStyleElement()
              .reduce(
                (
                  css,
                  {
                    props: {
                      dangerouslySetInnerHTML: { __html = '' } = {},
                    } = {},
                  } = {}
                ) => `${css}${__html}`,
                ''
              ),
          }}
        />
      );

      return {
        ...page,
        title,
        url: req.url,
        ampScriptTags,
        ampStyleTag,
      };
    } else {
      const page = renderPage(App => props =>
        sheet.collectStyles(<App {...props} />)
      );
      const styleTags = sheet.getStyleElement();

      return { ...page, styleTags };
    }
  }

  render() {
    if (is_AMP) {
      const { title, url, ampScriptTags, ampStyleTag, html } = this.props;

      return (
        <Amp.Html specName="html ⚡ for top-level html" lang="en">
          <head>
            {title}
            {headerBoilerplate(url)}
            {ampScriptTags}
            {ampStyleTag}
          </head>
          <body>
            <Main />
            {globals && (
              <script dangerouslySetInnerHTML={{ __html: globals }} />
            )}
            <NextScript />
          </body>
        </Amp.Html>
      );
    } else {
      return (
        <html>
          <Head>
            <meta charSet="utf-8" />
            <title>VisitDenmark</title>
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta
              name="viewport"
              content="width=device-width, initial-scale=1"
            />
            <link rel="icon" href={favicon} type="image/x-icon" />
            <style>{normalize}</style>
            <style>{globalStyle}</style>
            {this.props.styleTags}
          </Head>
          <body>
            <Main />
            {globals && (
              <script dangerouslySetInnerHTML={{ __html: globals }} />
            )}
            <NextScript />
          </body>
        </html>
      );
    }
  }
}
